package com.hke.morningstart.database

import android.annotation.SuppressLint
import android.content.Context
import androidx.room.*
import com.blankj.utilcode.util.Utils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

@Entity(tableName = "suggestions", indices = [Index(value = ["search"], unique = true)])
class SearchHistory(
    val search: String, val date: Long
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}

@Dao
interface SearchHistoryDao {
    @Delete
    fun delete(history: SearchHistory): Int

    @Query("SELECT * FROM suggestions where search LIKE :prefix  ORDER BY date DESC  LIMIT :limit")
    fun getSuggestions(prefix: String, limit: Int = 10): List<SearchHistory>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addQuery(history: SearchHistory)
}

@Database(entities = [SearchHistory::class], version = 1, exportSchema = false)
abstract class SearchDatabase : RoomDatabase() {
    abstract fun searchDao(): SearchHistoryDao

    companion object {
        @Volatile
        private var INSTANCE: SearchDatabase? = null
        private const val DATABASE_NAME = "search-db"
        fun getInstance(context: Context): SearchDatabase {
            if (INSTANCE == null) {
                synchronized(SearchDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            SearchDatabase::class.java, DATABASE_NAME
                        )
                            .fallbackToDestructiveMigration() //增加版本，启用 fallback to destructive migration — 数据库被清空
                            .build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}


class SearchApi {
    private val searchDao = SearchDatabase.getInstance(Utils.getApp()).searchDao()

    companion object {
        private var api: SearchApi? = null
            get() {
                if (field == null) {
                    field = SearchApi()
                }
                return field
            }

        fun getInstance(): SearchApi {
            return api!!
        }
    }

    fun addQuery(query: String) {
        Observable.just(query).filter {
            it.isNotEmpty()
        }.map {
            val data = SearchHistory(it, System.currentTimeMillis())
            searchDao.addQuery(data)
        }.subscribeOn(Schedulers.io()).subscribe()
    }

    @JvmOverloads
    fun deleteQuery(query: SearchHistory, callback: Consumer<Any>? = null) {
        Observable.just(query).map {
            searchDao.delete(query)
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(callback)
    }

    @SuppressLint("CheckResult")
    fun getSuggestions(prefix: String, callback: Consumer<List<SearchHistory>>) {
        Observable.just(prefix)
            .map {
                searchDao.getSuggestions("%$it%")
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(callback)
    }

}
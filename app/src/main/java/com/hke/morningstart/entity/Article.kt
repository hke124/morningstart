package com.hke.morningstart.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.jsoup.nodes.Element

@Parcelize
data class Article(val id: String, var title: String?, var author: String?, var time: String?) :
    Parcelable {
    constructor(e: Element) : this(e.select("a")?.attr("href")?.takeIf { it.length > 1 }
        ?.substring(1) ?: "", null, null, null) {
        if (id.isNotEmpty()) {
            e.select("td")?.apply {
                title = firstOrNull()?.text()?.trim()
                time = lastOrNull()?.text()?.trim()
                author = takeIf { size > 1 }?.get(1)?.text()?.trim()
            }
        }
    }
}
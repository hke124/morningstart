package com.hke.morningstart.okhttp

import android.os.Parcelable
import com.chad.library.adapter.base.module.BaseLoadMoreModule
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
class ResultWrapper<T>(
    val data: @RawValue T?,
    val code: LoadState = LoadState.Loading,
    val msg: String? = null
) : Parcelable {

    companion object {
        fun <T> success(data: T): ResultWrapper<T> = ResultWrapper(data, LoadState.Success.Complete)
        fun <T> error(msg: String?): ResultWrapper<T> = ResultWrapper(null, LoadState.Error(msg))
        fun <T> loading(): ResultWrapper<T> = ResultWrapper(null)
    }
}

class PagingRequest<T>(
    val index: Int? = 1,
    val key: T? = null,
    val loadSize: Int = 10,
    val loadMoreModule: BaseLoadMoreModule? = null
)

class PagingEntity<T>(val list: MutableList<T>, val total: Int = 0)

package com.hke.morningstart.okhttp
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class LoadState  : Parcelable {

    @Parcelize
    class Success(val endOfPaginationReached: Boolean) : LoadState() {
        override fun toString(): String = "Success(endOfPaginationReached=$endOfPaginationReached)"
        override fun equals(other: Any?): Boolean = other is Success && endOfPaginationReached == other.endOfPaginationReached
        override fun hashCode(): Int = endOfPaginationReached.hashCode()

        internal companion object {
            internal val Complete = Success(endOfPaginationReached = true)
            internal val Incomplete = Success(endOfPaginationReached = false)
        }
    }

    @Parcelize
    object Loading : LoadState() {
        override fun toString(): String = "Loading"
        override fun equals(other: Any?): Boolean = other is Loading
    }

    @Parcelize
    class Error(val error: String?) : LoadState() {
        override fun equals(other: Any?): Boolean = other is Error
        override fun hashCode(): Int = error.hashCode()
        override fun toString(): String = error.toString()
    }
}


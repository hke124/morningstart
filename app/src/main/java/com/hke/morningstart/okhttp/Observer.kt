package com.hke.morningstart.okhttp

import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.blankj.utilcode.util.ToastUtils
import com.blankj.utilcode.util.Utils
import com.hke.morningstart.OnProgressDialogListener
import com.hke.morningstart.R
import com.hke.morningstart.empty
import com.hke.morningstart.widget.LoadingTip

abstract class WithDialogObserver<T>(
    val dialog: OnProgressDialogListener? = null,
    val message: String = Utils.getApp().getString(
        R.string.network_loading
    ),
    val loadingTip: LoadingTip? = null,
    val swip: SwipeRefreshLayout? = null
) : Observer<ResultWrapper<T>> {
    override fun onChanged(t: ResultWrapper<T>?) {
        t?.apply {
            when (code) {
                is LoadState.Loading -> {
                    dialog?.startDialog(message)
                    swip?.apply {
                        isRefreshing=true
                        loadingTip?.setLoadingTip(LoadingTip.LoadStatus.finish)
                    }
                }
                is LoadState.Error -> {
                    dialog?.stopDialog()
                    val errorMsg = code.toString()
                    loadingTip?.setLoadingTip(LoadingTip.LoadStatus.error)
                    swip?.isRefreshing=false
                    if (!onFail(errorMsg)) {
                        ToastUtils.showShort(errorMsg)
                    }
                }
                else -> {
                    dialog?.stopDialog()
                    swip?.isRefreshing=false
                    loadingTip?.setLoadingTip(data?.takeUnless { empty() }?.let { LoadingTip.LoadStatus.finish } ?: LoadingTip.LoadStatus.empty)
                    onSuccess(data!!)
                }
            }
        }
    }

    abstract fun onSuccess(date: T)

    open fun onFail(msg: String?): Boolean {
        return false
    }
}
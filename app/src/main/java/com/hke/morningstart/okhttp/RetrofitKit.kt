package com.hke.morningstart.okhttp

import android.webkit.CookieManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.blankj.utilcode.util.Utils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.hke.morningstart.BuildConfig
import com.hke.morningstart.R
import com.hke.morningstart.entity.Article
import com.hke.morningstart.jsoup
import com.hke.morningstart.widget.ContentLayout
import kotlinx.android.synthetic.main.widget_content_layout.view.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


fun debug(call: () -> Unit) {
    if (BuildConfig.DEBUG) call()
}

const val host = "https://www.morningstar.cn/"
const val url_checkcode =host+ "membership/checkimg.aspx?0.12032911048346051"
val okhttp = OkHttpClient.Builder()
    .connectTimeout(10, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(10, TimeUnit.SECONDS)
    .cache(Cache(Utils.getApp().cacheDir, 1024 * 1024 * 256))
    .cookieJar(WebkitCookieJar(CookieManager.getInstance()))
    .apply {
        debug {
            addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }
    }
    .build()


val retrofit = Retrofit.Builder()
    .client(okhttp)
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(host)
    .build()

inline fun <reified T> getService(service: Class<T>): T {
    return retrofit.create(service)
}

class WebkitCookieJar(private val cm: CookieManager) : CookieJar {
    override fun loadForRequest(url: HttpUrl): List<Cookie> =
        cm.getCookie(url.toString())?.split("; ")?.mapNotNull {
            Cookie.parse(
                url,
                it
            )
        }?.toList()
            ?: emptyList()

    override fun saveFromResponse(url: HttpUrl, cookies: List<Cookie>) {
        cookies.forEach { cookie ->
            cm.setCookie(url.toString(), cookie.toString())
        }
    }
}


class Paging<K : Any, V : Any>(
    private val handle: SavedStateHandle,
    private val k: PagingRequest<K>,
    private val factory: suspend (PagingRequest<K>) -> PagingEntity<V>?
) {
    var index: Int?
        get() = if (handle.contains("index")) handle["index"] else k.index
        set(value) = handle.set("index", value)

    var hasNext: Boolean = false
    val result = MutableLiveData<ResultWrapper<PagingEntity<V>>>()
    var refresh = true
    private val mutex = Mutex()

    suspend fun query(refresh: Boolean = false) {
        mutex.withLock {
            this.refresh = refresh
            if (result.value?.code is LoadState.Loading) return
            if (refresh) handle.remove<Int>("index")
            if (index == null) return
            if (refresh) result.value = ResultWrapper.loading()
        }
        try {
            factory(PagingRequest(index, k.key, k.loadSize))?.let {
                hasNext = index!! + 1 <= it.total
                if (hasNext) {
                    index = index!! + 1
                }
                result.postValue(ResultWrapper.success(it))
            } ?: error()
        } catch (e: Exception) {
            e.printStackTrace()
            error(e.message)
        }
    }

    //需要放在数据加载之后
    fun success() {
        if (hasNext) {
            k.loadMoreModule?.loadMoreComplete()
        } else {
            k.loadMoreModule?.loadMoreEnd(refresh)
        }
    }

    private fun error(msg: String? = Utils.getApp().getString(R.string.network_failed)) {
        result.error(msg) {
            k.loadMoreModule?.loadMoreFail()
        }
    }

    fun observe(owner: LifecycleOwner, adapter: BaseQuickAdapter<V, *>, layout: ContentLayout) {
        result.observe(owner,
            object : WithDialogObserver<PagingEntity<V>>(
                loadingTip = layout.loadingTip,
                swip = layout.swipe
            ) {
                override fun onSuccess(date: PagingEntity<V>) {
                    if (refresh) {
                        adapter.setList(date.list)
                        layout.recycler.scrollToPosition(0)
                    } else {
                        adapter.addData(date.list)
                    }
                    success()
                }
            })
    }
}


class Subscriber<V : Any, K : Any>(
    handle: SavedStateHandle,
    private val factory: suspend (V) -> K?
) {
    val result = MutableLiveData<ResultWrapper<K>>()
    private val mutex = Mutex()

    suspend fun query(value: V) {
        mutex.withLock {
            if (result.value?.code is LoadState.Loading) return
            result.setValue(ResultWrapper.loading())
        }
        try {
            factory(value)?.let {
                result.postValue(ResultWrapper.success(it))
            } ?: result.error()
        } catch (e: Exception) {
            e.printStackTrace()
            result.error(e.message)
        }
    }
}

fun <T> MutableLiveData<ResultWrapper<T>>.error(
    msg: String? = Utils.getApp().getString(R.string.network_failed), callback: (() -> Unit)? = null
) = this.apply {
    if (callback != null) {
        callback()
    }
    postValue(ResultWrapper.error(msg))
}

fun ResponseBody?.pagingArticle(): PagingEntity<Article> {
    val dom = this?.string()?.jsoup()
    val articles =
        dom?.select("table")?.select("tr")?.map { o -> Article(o) }
            ?.filter { it.id.isNotEmpty() }?.toMutableList()
    val total: Int =
        dom?.select("#ctl00_ctl00_cphMain_cphArticleMain_AspNetPager1")?.select("a")?.let {
            try {
                if (it.size > 4) it[it.size - 3].text().toInt() else 0
            } catch (e: Exception) {
                0
            }
        } ?: 0
    return PagingEntity(articles ?: ArrayList(), total)
}
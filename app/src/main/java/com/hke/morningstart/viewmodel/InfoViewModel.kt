package com.hke.morningstart.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.hke.morningstart.okhttp.AccountRepo
import com.hke.morningstart.okhttp.Subscriber

class InfoViewModel(handle: SavedStateHandle, args: Bundle?) : ViewModel() {
    private val repo = AccountRepo.getInstance()
    val web = Subscriber<String, Pair<String, String>>(handle) { value ->
        repo.article(value)
    }
}

class InfoViewModelFactory(
    owner: SavedStateRegistryOwner,
    private val defaultArgs: Bundle? = null
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T = InfoViewModel(handle, defaultArgs) as T
}
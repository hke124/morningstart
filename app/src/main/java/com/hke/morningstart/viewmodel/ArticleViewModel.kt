package com.hke.morningstart.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.hke.morningstart.R
import com.hke.morningstart.okhttp.AccountRepo
import com.hke.morningstart.okhttp.Paging
import com.hke.morningstart.okhttp.PagingRequest

class ArticleViewModel(
    private val handle: SavedStateHandle,
    args: Bundle?,
    request: PagingRequest<Any>
) : ViewModel() {
    private val repo = AccountRepo.getInstance()
    private val fundschool = Paging(handle, request) {
        repo.fundschool(it)
    }
    private val advancedfund = Paging(handle, request) {
        repo.advancedfund(it)
    }
    private val stockfund = Paging(handle, request) {
        repo.stockfund(it)
    }
    private val mixfund = Paging(handle, request) {
        repo.mixfund(it)
    }
    private val bondfund = Paging(handle, request) {
        repo.bondfund(it)
    }
    private val currencyfund = Paging(handle, request) {
        repo.currencyfund(it)
    }
    private val guaranteedfund = Paging(handle, request) {
        repo.guaranteedfund(it)
    }
    private val closeendfund = Paging(handle, request) {
        repo.closeendfund(it)
    }
    private val etf = Paging(handle, request) {
        repo.etf(it)
    }
    private val qdii = Paging(handle, request) {
        repo.qdii(it)
    }
    private val gradedfund = Paging(handle, request) {
        repo.gradedfund(it)
    }
    private val observefund = Paging(handle, request) {
        repo.observefund(it)
    }
    private val overview = Paging(handle, request) {
        repo.overview(it)
    }
    private val words = Paging(handle, request) {
        repo.words(it)
    }
    val fundMap = mapOf(
        R.id.nav_homepage to fundschool,
        R.id.nav_news to advancedfund,
        R.id.types_stock to stockfund,
        R.id.types_mix to mixfund,
        R.id.types_bond to bondfund,
        R.id.types_currency to currencyfund,
        R.id.types_guaranteed to guaranteedfund,
        R.id.types_close_end to closeendfund,
        R.id.types_etf to etf,
        R.id.types_qdii to qdii,
        R.id.types_graded to gradedfund,
        R.id.nav_observe to observefund,
        R.id.nav_overview to overview,
        R.id.nav_words to words,
    )
}

class ArticleViewModelFactory(
    owner: SavedStateRegistryOwner,
    private val args: Bundle? = null,
    val request: PagingRequest<Any>
) :
    AbstractSavedStateViewModelFactory(owner, args) {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return ArticleViewModel(handle, args, request) as T
    }
}
package com.hke.morningstart.ui

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.*
import android.webkit.CookieManager
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.hke.morningstart.R
import kotlinx.android.synthetic.main.activity_web.*
import com.hke.morningstart.arguments
import kotlinx.android.synthetic.main.fragment_web.*

class WebActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, supportFragmentManager.findFragmentById(R.id.container)
                ?.let { it as? WebFragment }
                ?: WebFragment().arguments(intent.extras))
            .commit()
    }

    override fun onBackPressed() {
        supportFragmentManager.findFragmentById(R.id.container)
            ?.let { it as? WebFragment }
            ?.takeIf { it.onBackPressed() }
            ?: super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            super.onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}

class WebViewModel(handle: SavedStateHandle, args: Bundle?) : ViewModel() {
    val busy = handle.getLiveData("busy", false)
    val uri = handle.getLiveData("url", args?.getString("url")!!)
}

class WebViewModelFactory(owner: SavedStateRegistryOwner, private val defaultArgs: Bundle? = null) :
    AbstractSavedStateViewModelFactory(owner, defaultArgs) {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T = WebViewModel(handle, defaultArgs) as T
}

class WebFragment : Fragment() {
    private val viewModel: WebViewModel by viewModels {
        WebViewModelFactory(
            this,
            bundleOf("url" to defuri)
        )
    }

    private val defuri: String
        get() = arguments?.takeIf { it.containsKey("url") }?.getString("url")
            ?: ""

    fun onBackPressed(): Boolean {
        if (web.canGoBack()) {
            web.goBack()
            return true
        }
        return false
    }


    override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        setHasOptionsMenu(true)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        web.destroy()
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_web, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        CookieManager.getInstance().acceptThirdPartyCookies(web)
        viewModel.busy.observe(viewLifecycleOwner) { swipe.isRefreshing = it }
        swipe.setOnRefreshListener { web.loadUrl(viewModel.uri.value!!) }

        val settings = web.settings
        settings.javaScriptEnabled = true
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        val back = button2
        val fore = button3
        web.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                return false
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                viewModel.busy.postValue(true)
                progress.progress = 0
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                viewModel.busy.postValue(false)
                progress.progress = 100
                viewModel.uri.postValue(url)

                back.isEnabled = view?.canGoBack() ?: false
                fore.isEnabled = view?.canGoForward() ?: false
            }
        }
        web.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                progress.progress = newProgress
            }
        }

        val click = View.OnClickListener { v ->
            when (v?.id) {
                R.id.button1 -> web.loadUrl(defuri)
                R.id.button2 -> if (web.canGoBack()) web.goBack()
                R.id.button3 -> if (web.canGoForward()) web.goForward()
                R.id.button4 -> web.loadUrl(viewModel.uri.value!!)
            }
        }
        listOf(button1, button2, button3, button4)
            .forEach { it.setOnClickListener(click) }
        web.loadUrl(viewModel.uri.value!!)
    }
}
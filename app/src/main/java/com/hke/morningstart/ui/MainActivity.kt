package com.hke.morningstart.ui

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.annotation.IntDef
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.blankj.utilcode.util.ColorUtils
import com.blankj.utilcode.util.KeyboardUtils
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.listener.OnLoadMoreListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.hippo.yorozuya.AnimationUtils
import com.hippo.yorozuya.SimpleAnimatorListener
import com.hke.morningstart.R
import com.hke.morningstart.adapter.ArticleAdapter
import com.hke.morningstart.okhttp.PagingRequest
import com.hke.morningstart.view.AddDeleteDrawable
import com.hke.morningstart.view.BringOutTransition
import com.hke.morningstart.view.MenuArrowDrawable
import com.hke.morningstart.view.ViewTransition
import com.hke.morningstart.viewmodel.ArticleViewModel
import com.hke.morningstart.viewmodel.ArticleViewModelFactory
import com.hke.morningstart.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.widget_content_layout.view.*

const val STATE_NORMAL = 0
const val STATE_SIMPLE_SEARCH = 1
const val STATE_SEARCH = 2
const val STATE_SEARCH_SHOW_LIST = 3
private const val BACK_PRESSED_INTERVAL = 2000

class MainActivity : AppCompatActivity(), SearchBar.Helper, FastScroller.OnDragHandlerListener,
    SearchBarMover.Helper, SearchBar.OnStateChangeListener, FabLayout.OnClickFabListener,
    FabLayout.OnExpandListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener,
    OnLoadMoreListener, NavigationView.OnNavigationItemSelectedListener {
    private val ANIMATE_TIME = 300L
    private val mHideActionFabSlop = 0
    private var mShowActionFab = true
    private var mPressBackTime: Long = 0
    private lateinit var mViewTransition: ViewTransition
    private lateinit var mSearchBarMover: SearchBarMover

    private lateinit var mLeftDrawable: MenuArrowDrawable
    private lateinit var mRightDrawable: AddDeleteDrawable
    private lateinit var mActionFabDrawable: AddDeleteDrawable
    private val itemAdapter = ArticleAdapter()

    private var mNavCheckedItem = R.id.nav_homepage
    private val viewModel: ArticleViewModel by viewModels {
        ArticleViewModelFactory(
            this,
            request = PagingRequest(loadMoreModule = itemAdapter.loadMoreModule.apply {
                setOnLoadMoreListener(this@MainActivity)
            })
        )
    }

    @State
    private var mState: Int = STATE_NORMAL
    private val mSearchFabAnimatorListener: Animator.AnimatorListener =
        object : SimpleAnimatorListener() {
            override fun onAnimationEnd(animation: Animator) {
                if (null != search_fab) {
                    search_fab.visibility = View.INVISIBLE
                }
            }
        }
    private val mActionFabAnimatorListener: Animator.AnimatorListener =
        object : SimpleAnimatorListener() {
            override fun onAnimationEnd(animation: Animator) {
                if (null != fab_layout) {
                    (fab_layout.primaryFab as View).visibility = View.INVISIBLE
                }
            }
        }

    private val mOnScrollListener: RecyclerView.OnScrollListener =
        object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {}
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy >= mHideActionFabSlop) {
                    hideActionFab()
                } else if (dy <= -mHideActionFabSlop / 2) {
                    showActionFab()
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initData()
    }

    private fun initView() {
        search_fab.apply {
            setOnClickListener {
                search_bar.applySearch()
                KeyboardUtils.hideSoftInput(this@MainActivity)
            }
        }
        val paddingTopSB = resources.getDimensionPixelOffset(R.dimen.gallery_padding_top_search_bar)
        mViewTransition = BringOutTransition(content_layout, search_layout)


        content_layout.recycler.apply {
            addOnScrollListener(mOnScrollListener)
            clipToPadding = false
            clipChildren = false
        }
        content_layout.setFitPaddingTop(paddingTopSB)
        content_layout.fast_scroller.let {
            it.setPadding(
                it.paddingLeft, it.paddingTop + paddingTopSB,
                it.paddingRight, it.paddingBottom
            )
            it.setOnDragHandlerListener(this)
        }

        mLeftDrawable = MenuArrowDrawable(this)
        mRightDrawable = AddDeleteDrawable(this, ColorUtils.getColor(R.color.primary_drawable))
        mActionFabDrawable =
            AddDeleteDrawable(this, ColorUtils.getColor(R.color.primary_drawable_dark))
        search_bar.apply {
            setLeftDrawable(mLeftDrawable)
            setRightDrawable(mRightDrawable)
            setHelper(this@MainActivity)
            setOnStateChangeListener(this@MainActivity)
            setEditTextHint(getString(R.string.search))
        }
        fab_layout.apply {
            setAutoCancel(true)
            setExpanded(false)
            setHidePrimaryFab(false)
            setOnClickFabListener(this@MainActivity)
            setOnExpandListener(this@MainActivity)
            primaryFab?.setImageDrawable(mActionFabDrawable)
        }
        search_fab.setOnClickListener(this)

        mSearchBarMover = SearchBarMover(this, search_bar, content_layout.recycler, search_layout)

        // Restore state
        val newState = mState
        mState = STATE_NORMAL
        setState(newState, false)
        nav_view.setCheckedItem(mNavCheckedItem)
        nav_view.setNavigationItemSelectedListener(this)
    }

    private fun initData() {
        itemAdapter.apply {
            setEmptyView(content_layout.loadingTip)
            setOnItemClickListener { _, _, position ->
                startActivity(
                    Intent(this@MainActivity, InfoActivity::class.java).putExtra(
                        "url",
                        this.getItem(position).id
                    )
                )
            }
        }
        content_layout.recycler.adapter = itemAdapter
        content_layout.refreshListener = this
        onRefresh()
        viewModel.fundMap.values.forEach {
            it.observe(this, itemAdapter, content_layout)
        }
    }

    private fun hideActionFab() {
        if (null != fab_layout && STATE_NORMAL == mState && mShowActionFab) {
            mShowActionFab = false
            fab_layout.primaryFab?.apply {
                animate().scaleX(0.0f).scaleY(0.0f).setListener(mActionFabAnimatorListener)
                    .setDuration(ANIMATE_TIME)
                    .setStartDelay(0L)
                    .setInterpolator(AnimationUtils.SLOW_FAST_INTERPOLATOR).start()
            }
        }
    }

    private fun selectSearchFab(animation: Boolean) {
        if (null == fab_layout || null == search_fab) {
            return
        }
        mShowActionFab = false
        if (animation) {
            var delay = 0L
            fab_layout.primaryFab?.apply {
                if (View.INVISIBLE == visibility) {
                    delay = 0L
                } else {
                    delay = ANIMATE_TIME
                    animate().scaleX(0.0f).scaleY(0.0f).setListener(mActionFabAnimatorListener)
                        .setDuration(ANIMATE_TIME)
                        .setStartDelay(0L)
                        .setInterpolator(AnimationUtils.SLOW_FAST_INTERPOLATOR).start()
                }
            }
            search_fab.visibility = View.VISIBLE
            search_fab.rotation = -45.0f
            search_fab.animate().scaleX(1.0f).scaleY(1.0f).rotation(0.0f).setListener(null)
                .setDuration(ANIMATE_TIME)
                .setStartDelay(delay)
                .setInterpolator(AnimationUtils.FAST_SLOW_INTERPOLATOR).start()
        } else {
            fab_layout.setExpanded(false, false)
            fab_layout.primaryFab?.apply {
                visibility = View.INVISIBLE
                scaleX = 0.0f
                scaleY = 0.0f
            }
            search_fab.visibility = View.VISIBLE
            search_fab.scaleX = 1.0f
            search_fab.scaleY = 1.0f
        }
    }

    private fun selectActionFab(animation: Boolean) {
        if (null == fab_layout || null == search_fab) {
            return
        }
        mShowActionFab = true
        if (animation) {
            val delay: Long
            if (View.INVISIBLE == search_fab.getVisibility()) {
                delay = 0L
            } else {
                delay = ANIMATE_TIME
                search_fab.animate().scaleX(0.0f).scaleY(0.0f)
                    .setListener(mSearchFabAnimatorListener)
                    .setDuration(ANIMATE_TIME)
                    .setStartDelay(0L)
                    .setInterpolator(AnimationUtils.SLOW_FAST_INTERPOLATOR).start()
            }
            fab_layout.primaryFab?.apply {
                visibility = View.VISIBLE
                rotation = -45.0f
                animate().scaleX(1.0f).scaleY(1.0f).rotation(0.0f).setListener(null)
                    .setDuration(ANIMATE_TIME)
                    .setStartDelay(delay)
                    .setInterpolator(AnimationUtils.FAST_SLOW_INTERPOLATOR).start()
            }

        } else {
            fab_layout.setExpanded(false, false)
            fab_layout.primaryFab?.apply {
                visibility = View.VISIBLE
                scaleX = 1.0f
                scaleY = 1.0f
            }

            search_fab.visibility = View.INVISIBLE
            search_fab.scaleX = 0.0f
            search_fab.scaleY = 0.0f
        }
    }

    private fun setState(@State state: Int) {
        setState(state, true)
    }

    private fun setState(
        @State state: Int,
        animation: Boolean
    ) {
        if (null == search_bar || null == search_layout) {
            return
        }
        if (mState != state) {
            val oldState = mState
            mState = state
            when (oldState) {
                STATE_NORMAL -> if (state == STATE_SIMPLE_SEARCH) {
                    search_bar.setState(SearchBar.STATE_SEARCH_LIST, animation)
                    mSearchBarMover.returnSearchBarPosition()
                    selectSearchFab(animation)
                } else if (state == STATE_SEARCH) {
                    mViewTransition.showView(1, animation)
                    search_layout.scrollSearchContainerToTop()
                    search_bar.setState(SearchBar.STATE_SEARCH, animation)
                    mSearchBarMover.returnSearchBarPosition()
                    selectSearchFab(animation)
                } else if (state == STATE_SEARCH_SHOW_LIST) {
                    mViewTransition.showView(1, animation)
                    search_layout.scrollSearchContainerToTop()
                    search_bar.setState(SearchBar.STATE_SEARCH_LIST, animation)
                    mSearchBarMover.returnSearchBarPosition()
                    selectSearchFab(animation)
                }
                STATE_SIMPLE_SEARCH -> if (state == STATE_NORMAL) {
                    search_bar.setState(SearchBar.STATE_NORMAL, animation)
                    mSearchBarMover.returnSearchBarPosition()
                    selectActionFab(animation)
                } else if (state == STATE_SEARCH) {
                    mViewTransition.showView(1, animation)
                    search_layout.scrollSearchContainerToTop()
                    search_bar.setState(SearchBar.STATE_SEARCH, animation)
                    mSearchBarMover.returnSearchBarPosition()
                } else if (state == STATE_SEARCH_SHOW_LIST) {
                    mViewTransition.showView(1, animation)
                    search_layout.scrollSearchContainerToTop()
                    search_bar.setState(SearchBar.STATE_SEARCH_LIST, animation)
                    mSearchBarMover.returnSearchBarPosition()
                }
                STATE_SEARCH -> if (state == STATE_NORMAL) {
                    mViewTransition.showView(0, animation)
                    search_bar.setState(SearchBar.STATE_NORMAL, animation)
                    mSearchBarMover.returnSearchBarPosition()
                    selectActionFab(animation)
                } else if (state == STATE_SIMPLE_SEARCH) {
                    mViewTransition.showView(0, animation)
                    search_bar.setState(SearchBar.STATE_SEARCH_LIST, animation)
                    mSearchBarMover.returnSearchBarPosition()
                } else if (state == STATE_SEARCH_SHOW_LIST) {
                    search_bar.setState(SearchBar.STATE_SEARCH_LIST, animation)
                    mSearchBarMover.returnSearchBarPosition()
                }
                STATE_SEARCH_SHOW_LIST -> if (state == STATE_NORMAL) {
                    mViewTransition.showView(0, animation)
                    search_bar.setState(SearchBar.STATE_NORMAL, animation)
                    mSearchBarMover.returnSearchBarPosition()
                    selectActionFab(animation)
                } else if (state == STATE_SIMPLE_SEARCH) {
                    mViewTransition.showView(0, animation)
                    search_bar.setState(SearchBar.STATE_SEARCH_LIST, animation)
                    mSearchBarMover.returnSearchBarPosition()
                } else if (state == STATE_SEARCH) {
                    search_bar.setState(SearchBar.STATE_SEARCH, animation)
                    mSearchBarMover.returnSearchBarPosition()
                }
            }
        }
    }

    override fun onClickTitle() {
        if (mState == STATE_NORMAL) {
            setState(STATE_SIMPLE_SEARCH)
        }
    }

    override fun onClickLeftIcon() {
        if (search_bar.state == SearchBar.STATE_NORMAL) {
            toggleDrawer(Gravity.LEFT)
        } else {
            setState(STATE_NORMAL)
        }
    }

    override fun onClickRightIcon() {
        if (search_bar.state == SearchBar.STATE_NORMAL) {
            setState(STATE_SEARCH)
        } else {
            if (search_bar.editText.length() == 0) {
                setState(STATE_NORMAL)
            } else {
                // Clear
                search_bar.text = ""
            }
        }
    }

    override fun onSearchEditTextClick() {
        if (mState == STATE_SEARCH) {
            setState(STATE_SEARCH_SHOW_LIST)
        }
    }

    override fun onApplySearch(query: String?) {
        if (mState == STATE_SEARCH || mState == STATE_SEARCH_SHOW_LIST) {

        }
        onRefresh()
        setState(STATE_NORMAL)
    }

    override fun onSearchEditTextBackPressed() {
        onBackPressed()
    }

    private fun checkDoubleClickExit(): Boolean {
        val time = System.currentTimeMillis()
        return if (time - mPressBackTime > BACK_PRESSED_INTERVAL) {
            // It is the last scene
            mPressBackTime = time
            ToastUtils.showShort(R.string.press_twice_exit)
            true
        } else {
            false
        }
    }

    override fun onBackPressed() {
        if (null != fab_layout && fab_layout.isExpanded) {
            fab_layout.setExpanded(false)
            return
        }
        val handle: Boolean
        when (mState) {
            STATE_NORMAL -> handle =
                checkDoubleClickExit()
            STATE_SIMPLE_SEARCH -> {
                setState(STATE_NORMAL)
                handle = true
            }
            STATE_SEARCH -> {
                setState(STATE_NORMAL)
                handle = true
            }
            STATE_SEARCH_SHOW_LIST -> {
                setState(STATE_SEARCH)
                handle = true
            }
            else -> handle = checkDoubleClickExit()
        }
        if (!handle) {
            if (draw_view.isDrawerOpen(Gravity.LEFT)) {
                draw_view.closeDrawers()
            } else {
                finish()
            }
        }
    }


    @IntDef(
        STATE_NORMAL,
        STATE_SIMPLE_SEARCH,
        STATE_SEARCH,
        STATE_SEARCH_SHOW_LIST
    )
    @Retention(
        AnnotationRetention.SOURCE
    )
    private annotation class State

    override fun onStartDragHandler() {
    }

    override fun onEndDragHandler() {
    }

    override fun isValidView(recyclerView: RecyclerView?): Boolean {
        return (mState == STATE_NORMAL && recyclerView == content_layout.recycler) ||
                (mState == STATE_SEARCH && recyclerView == search_layout)
    }

    override fun getValidRecyclerView(): RecyclerView? {
        if (mState == STATE_NORMAL || mState == STATE_SIMPLE_SEARCH) {
            return content_layout.recycler
        } else {
            return search_layout
        }
    }

    override fun forceShowSearchBar(): Boolean {
        return mState == STATE_SIMPLE_SEARCH || mState == STATE_SEARCH_SHOW_LIST

    }

    override fun onStateChange(
        searchBar: SearchBar?,
        newState: Int,
        oldState: Int,
        animation: Boolean
    ) {
        when (oldState) {
            SearchBar.STATE_NORMAL -> {
                mLeftDrawable.setMenu(if (animation) ANIMATE_TIME else 0)
                mRightDrawable.setDelete(if (animation) ANIMATE_TIME else 0)
            }
            SearchBar.STATE_SEARCH -> if (newState == SearchBar.STATE_NORMAL) {
                mLeftDrawable.setMenu(if (animation) ANIMATE_TIME else 0)
                mRightDrawable.setAdd(if (animation) ANIMATE_TIME else 0)
            }
            SearchBar.STATE_SEARCH_LIST -> if (newState == STATE_NORMAL) {
                mLeftDrawable.setMenu(if (animation) ANIMATE_TIME else 0)
                mRightDrawable.setAdd(if (animation) ANIMATE_TIME else 0)
            }
            else -> {
                mLeftDrawable.setArrow(if (animation) ANIMATE_TIME else 0)
                mRightDrawable.setDelete(if (animation) ANIMATE_TIME else 0)
            }
        }
    }

    override fun onExpand(expanded: Boolean) {
        if (expanded) {
            mActionFabDrawable.setDelete(ANIMATE_TIME)
        } else {
            mActionFabDrawable.setAdd(ANIMATE_TIME)
        }
    }

    override fun onClickPrimaryFab(view: FabLayout?, fab: FloatingActionButton?) {
        if (STATE_NORMAL == mState) {
            view?.toggle()
        }
    }

    override fun onClickSecondaryFab(view: FabLayout?, fab: FloatingActionButton?, position: Int) {
        when (position) {
            0 -> {
            }
            1 -> {
            }
            2 -> {
                onRefresh()
            }
        }
        view!!.setExpanded(false)
    }

    override fun onClick(p0: View?) {
        if (STATE_NORMAL != mState && null != search_bar) {
            search_bar.applySearch()
            KeyboardUtils.hideSoftInput(this@MainActivity)
        }
    }

    override fun onRefresh() {
        lifecycleScope.launchWhenCreated {
            viewModel.fundMap[mNavCheckedItem]?.query(true)
        }
    }

    override fun onLoadMore() {
        lifecycleScope.launchWhenCreated {
            viewModel.fundMap[mNavCheckedItem]?.query(false)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Don't select twice
        if (item.isChecked) {
            return false
        }
        mNavCheckedItem = item.itemId
        draw_view.closeDrawers()
        mSearchBarMover.showSearchBar(false)
        onRefresh()
        return true
    }

    private fun toggleDrawer(drawerGravity: Int) {
        if (draw_view.isDrawerOpen(drawerGravity)) {
            draw_view.closeDrawer(drawerGravity)
        } else {
            draw_view.openDrawer(drawerGravity)
        }
    }

    private fun showActionFab() {
        if (null != fab_layout && STATE_NORMAL == mState && !mShowActionFab) {
            mShowActionFab = true
            fab_layout.primaryFab?.apply {
                visibility = View.VISIBLE
                rotation = -45.0f
                animate().scaleX(1.0f).scaleY(1.0f).rotation(0.0f).setListener(null)
                    .setDuration(ANIMATE_TIME)
                    .setStartDelay(0L)
                    .setInterpolator(AnimationUtils.FAST_SLOW_INTERPOLATOR).start()
            }

        }
    }
}



package com.hke.morningstart.ui

import android.annotation.SuppressLint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.webkit.*
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.hke.morningstart.LoadingListener
import com.hke.morningstart.R
import com.hke.morningstart.loadingTip
import com.hke.morningstart.okhttp.WithDialogObserver
import com.hke.morningstart.okhttp.host
import com.hke.morningstart.okhttp.okhttp
import com.hke.morningstart.viewmodel.InfoViewModel
import com.hke.morningstart.viewmodel.InfoViewModelFactory
import com.hke.morningstart.widget.LoadingTip
import kotlinx.android.synthetic.main.activity_info.*
import java.util.*
import kotlin.collections.HashMap

class InfoActivity : AppCompatActivity(), LoadingListener, LoadingTip.LoadingTipListener {
    private val viewModel: InfoViewModel by viewModels { InfoViewModelFactory(this) }
    private val _url by lazy { intent.getStringExtra("url")!! }
    private val _loadingTip by lazy {
        loadingTip(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        content.addView(_loadingTip)
        CookieManager.getInstance().acceptThirdPartyCookies(web)
        val settings = web.settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
        @SuppressLint("SetJavaScriptEnabled")
        settings.javaScriptEnabled = true
        web.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                return false
            }

            override fun shouldInterceptRequest(
                view: WebView?,
                request: WebResourceRequest?
            ): WebResourceResponse? =
                when (request?.url?.scheme?.toLowerCase(Locale.getDefault())) {
                    "http", "https" -> try {
                        val call = okhttp3.Request.Builder().method(request.method, null)
                            .url(request.url.toString()).apply {
                                request.requestHeaders?.forEach { header(it.key, it.value) }
                            }.build()
                        val response = okhttp.newCall(call).execute()
                        WebResourceResponse(
                            response.header("Content-Type", "text/html; charset=UTF-8"),
                            response.header("Content-Encoding", "utf-8"),
                            response.code(),
                            response.message(),
                            response.headers().let {
                                HashMap<String, String>().apply {
                                    it.names().forEach { str ->
                                        this[str] = it.get(str) ?: ""
                                    }
                                }
                            },
                            response.body()?.byteStream()
                        )
                    } catch (_: Exception) {
                        null
                    }
                    else -> null
                } ?: super.shouldInterceptRequest(view, request)

            override fun onRenderProcessGone(
                view: WebView?,
                detail: RenderProcessGoneDetail?
            ): Boolean {
                web.destroy()
                return true
            }
        }

        viewModel.web.result.observe(
            this,
            object : WithDialogObserver<Pair<String, String>>(loadingTip = _loadingTip) {
                override fun onSuccess(date: Pair<String, String>) {
                    web.loadDataWithBaseURL(host, date.first, "text/html", "utf-8", null)
                    title = date.second
                }
            })
        query(_url)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun query(id: String) {
        lifecycleScope.launchWhenCreated {
            viewModel.web.query(id)
        }
    }

    override fun reload() {
        query(_url)
    }
}
package com.hke.morningstart.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.blankj.utilcode.util.KeyboardUtils
import com.blankj.utilcode.util.ToastUtils
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.hke.morningstart.GlideApp
import com.hke.morningstart.LoadingListener
import com.hke.morningstart.R
import com.hke.morningstart.okhttp.WithDialogObserver
import com.hke.morningstart.okhttp.url_checkcode
import com.hke.morningstart.viewmodel.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoadingListener {

    private val viewModel: LoginViewModel by viewModels { LoginViewModelFactory(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initView()
    }


    private fun initView() {
        sign_in.setOnClickListener(::signIn)
        viewModel.login.result.observe(this, object : WithDialogObserver<Any>(this) {
            override fun onSuccess(date: Any) {
                ToastUtils.showShort(date.toString())
                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                finish()
            }
        })
        queryCheckCode()
        iv_checkcode.setOnClickListener {
            queryCheckCode()
        }
    }


    private fun queryCheckCode() {
        val requestOptions =
            RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
        GlideApp.with(this).load(url_checkcode)
            .apply(requestOptions)
            .into(iv_checkcode)
    }

    private fun signIn(view: View) {
        val name: String = username.text.toString()
        val psw: String = password.text.toString()
        val checkcode:String= checkcode.text.toString()
        if (name.isEmpty()) {
            username_layout.error = getString(R.string.error_username_cannot_empty)
            return
        } else {
            username_layout.error = null
        }
        if (psw.isEmpty()) {
            password_layout.error = getString(R.string.error_password_cannot_empty)
            return
        } else {
            password_layout.error = null
        }

        if (checkcode.isEmpty()) {
            checkcode_layout.error = getString(R.string.error_checkcode_cannot_empty)
            return
        } else {
            checkcode_layout.error = null
        }
        KeyboardUtils.hideSoftInput(this)
        lifecycleScope.launchWhenCreated {
            viewModel.login.query(LoginRequest(name,psw ,checkcode))
        }
    }
}


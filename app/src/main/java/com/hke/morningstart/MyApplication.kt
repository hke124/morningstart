package com.hke.morningstart

import android.app.Application
import com.blankj.utilcode.util.Utils

class MyApplication : Application() {
    init {
        Utils.init(this)
    }
}
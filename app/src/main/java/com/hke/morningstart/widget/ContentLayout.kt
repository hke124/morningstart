package com.hke.morningstart.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.blankj.utilcode.util.SizeUtils
import com.hke.morningstart.R
import com.hke.morningstart.loadingTip
import com.hke.morningstart.view.HandlerDrawable
import kotlinx.android.synthetic.main.widget_content_layout.view.*

class ContentLayout : FrameLayout, LoadingTip.LoadingTipListener {
    @JvmOverloads
    constructor(
        context: Context,
        attributeSet: AttributeSet? = null,
        defStyleAttr: Int = 0
    ) : super(
        context,
        attributeSet,
        defStyleAttr
    ) {
        init(context)
    }

    private var mRecyclerViewOriginTop = 0
    private var mRecyclerViewOriginBottom = 0
    val loadingTip by lazy {
        loadingTip(context, this)
    }
    var refreshListener: SwipeRefreshLayout.OnRefreshListener? = null
        set(value) {
            field = value
            swipe.setOnRefreshListener(value)
        }

    private fun init(context: Context) {
        LayoutInflater.from(context).inflate(R.layout.widget_content_layout, this)

        fast_scroller.attachToRecyclerView(recycler)
        val drawable = HandlerDrawable()
        drawable.setColor(R.color.teal_500)
        fast_scroller.setHandlerDrawable(drawable)
        swipe.apply {
            setColorSchemeResources(
                R.color.loading_indicator_red,
                R.color.loading_indicator_purple,
                R.color.loading_indicator_blue,
                R.color.loading_indicator_cyan,
                R.color.loading_indicator_green,
                R.color.loading_indicator_yellow
            )
        }
        mRecyclerViewOriginTop = recycler.paddingTop
        mRecyclerViewOriginBottom = recycler.paddingBottom
    }

    fun showFastScroll() {
        if (!fast_scroller.isAttached) {
            fast_scroller.attachToRecyclerView(recycler)
        }
    }

    fun hideFastScroll() {
        fast_scroller.detachedFromRecyclerView()
    }

    fun setFitPaddingTop(fitPaddingTop: Int) {
        // RecyclerView
        recycler.setPadding(
            recycler.paddingLeft,
            mRecyclerViewOriginTop + fitPaddingTop,
            recycler.paddingRight,
            recycler.paddingBottom
        );
        // RefreshLayout
        swipe.setProgressViewOffset(true, 0, fitPaddingTop + SizeUtils.dp2px(32f)); // TODO
    }

    fun setFitPaddingBottom(fitPaddingBottom: Int) {
        // RecyclerView
        recycler.setPadding(
            recycler.paddingLeft,
            recycler.paddingTop, recycler.paddingRight,
            mRecyclerViewOriginBottom + fitPaddingBottom
        )
    }

    override fun reload() {
        refreshListener?.onRefresh()
    }

}
package com.hke.morningstart.widget

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.hke.morningstart.R
import kotlinx.android.synthetic.main.dialog_loading.*

class Loading(context: Context) : Dialog(context) {

    constructor(value: String, context: Context) : this(context) {
        toast_textview.text = value
    }

    init {
        window?.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(R.layout.dialog_loading)
            attributes.dimAmount = 0f
        }
        //背景不变暗
        setCancelable(false)
        setCanceledOnTouchOutside(false)
    }
}

private var mLoadingDialog: Loading? = null


/**
 * 显示加载对话框
 *
 * @param context    上下文
 * @param msg        对话框显示内容
 * @param cancelable 对话框是否可以取消
 * @param isCheck    检查是否存在,如果存在就不再加载新的dialog
 */
fun showDialogForLoading(
    context: Activity,
    msg: String?=null,
    cancelable: Boolean = true,
    isCheck: Boolean = false
): Dialog? {
    if (isCheck && mLoadingDialog != null && mLoadingDialog!!.isShowing) {
        return mLoadingDialog
    }
    cancelDialogForLoading()
    mLoadingDialog = Loading(msg ?: context.getString(R.string.network_loading), context).apply {
        setCancelable(cancelable)
        show()
    }
    return mLoadingDialog
}


/**
 * 关闭加载对话框
 */
fun cancelDialogForLoading() {
    mLoadingDialog?.takeIf { it.isShowing }?.cancel()
}
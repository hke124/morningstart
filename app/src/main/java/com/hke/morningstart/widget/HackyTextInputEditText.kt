/*
 * Copyright 2018 Hippo Seven
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hke.morningstart.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText
import java.lang.reflect.Field
import java.lang.reflect.Method

class HackyTextInputEditText(context: Context?, attrs: AttributeSet?) : TextInputEditText(
    context!!, attrs
) {
    companion object {
        private var HAS_METHOD_UPDATE_CURSOR_POSITION_MZ = false
        private var FIELD_M_HINT: Field? = null

        init {
            var hasMethodUpdateCursorPositionMz = false
            try {
                val clazz = ClassLoader.getSystemClassLoader().loadClass("android.widget.Editor")
                val methods: Array<Method> = clazz.declaredMethods
                for (method in methods) {
                    if ("updateCursorPositionMz" == method.name) {
                        hasMethodUpdateCursorPositionMz = true
                    }
                }
            } catch (e: Throwable) {
                e.printStackTrace()
            }
            var fieldMHint: Field? = null
            if (hasMethodUpdateCursorPositionMz) {
                try {
                    fieldMHint =
                        TextView::class.java.getDeclaredField("mHint")
                    fieldMHint.isAccessible = true
                } catch (e: Throwable) {
                    e.printStackTrace()
                }
            }
            HAS_METHOD_UPDATE_CURSOR_POSITION_MZ = hasMethodUpdateCursorPositionMz
            FIELD_M_HINT = fieldMHint
        }
    }

    override fun getHint(): CharSequence? {
        return if (HAS_METHOD_UPDATE_CURSOR_POSITION_MZ && FIELD_M_HINT != null) {
            try {
                superHint
            } catch (e: Throwable) {
                e.printStackTrace()
                super.getHint()
            }
        } else {
            super.getHint()
        }
    }

    @get:Throws(IllegalAccessException::class)
    private val superHint: CharSequence
        get() = FIELD_M_HINT!![this] as CharSequence
}
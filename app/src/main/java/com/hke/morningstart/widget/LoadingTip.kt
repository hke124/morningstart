package com.hke.morningstart.widget

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.widget.*
import com.hke.morningstart.R

/**
 * des:加载页面内嵌提示
 */
class LoadingTip : LinearLayout, View.OnClickListener {
    private var img_tip_logo: ImageView? = null
    private var progress: ProgressBar? = null
    private var tv_tips: TextView? = null
    private var bt_operate: Button? = null
    private val errorMsg: String? = null
    private var onReloadListener: LoadingTipListener? = null
    private var emptyData: Pair<Int, String>? = null

    constructor(context: Context?) : super(context) {
        initView(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initView(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView(context)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        initView(context)
    }

    override fun onClick(v: View) {
        if (onReloadListener != null) {
            setLoadingTip(LoadStatus.loading)
            onReloadListener!!.reload()
        }
    }

    //分为服务器失败，网络加载失败、数据为空、加载中、完成四种状态
    enum class LoadStatus {
        sereverError, error, empty, loading, finish, onlyText
    }

    private fun initView(context: Context?) {
        View.inflate(context, R.layout.widget_loading_tip, this)
        img_tip_logo = findViewById<View>(R.id.img_tip_logo) as ImageView?
        progress = findViewById<View>(R.id.progress) as ProgressBar?
        tv_tips = findViewById<View>(R.id.tv_tips) as TextView?
        bt_operate = findViewById<View>(R.id.bt_operate) as Button?
        //重新尝试
        bt_operate!!.setOnClickListener(this)
        visibility = View.VISIBLE
        setOnClickListener(this)
    }

    fun setTips(tips: String?) {
        tv_tips?.text = tips
    }

    fun setEmptyDate(content: String, resId: Int) {
        emptyData = Pair(resId, content)
        setLoadingTip(LoadStatus.empty)
    }

    /**
     * 根据状态显示不同的提示
     *
     * @param loadStatus
     */
    fun setLoadingTip(loadStatus: LoadStatus?) {
        when (loadStatus) {
            LoadStatus.empty -> {
                visibility = View.VISIBLE
                isEnabled = false
                img_tip_logo!!.visibility = View.VISIBLE
                progress!!.visibility = View.GONE
                if (emptyData == null) {
                    tv_tips?.text = context.getText(R.string.network_complete).toString()
                    img_tip_logo!!.setImageResource(R.drawable.error)
                } else {
                    tv_tips?.text = emptyData!!.second
                    img_tip_logo!!.setImageResource(emptyData!!.first)
                }
            }
            LoadStatus.sereverError -> {
                visibility = View.VISIBLE
                isEnabled = true
                img_tip_logo!!.visibility = View.VISIBLE
                progress!!.visibility = View.GONE
                if (TextUtils.isEmpty(errorMsg)) {
                    tv_tips?.text = context.getText(R.string.network_failed).toString()
                } else {
                    tv_tips?.text = errorMsg
                }
                img_tip_logo!!.setImageResource(R.drawable.error)
            }
            LoadStatus.error -> {
                visibility = View.VISIBLE
                isEnabled = true
                img_tip_logo!!.visibility = View.VISIBLE
                progress!!.visibility = View.GONE
                if (TextUtils.isEmpty(errorMsg)) {
                    tv_tips?.text = context.getText(R.string.network_failed).toString()
                } else {
                    tv_tips?.text = errorMsg
                }
                img_tip_logo!!.setImageResource(R.drawable.error)
            }
            LoadStatus.loading -> {
                visibility = View.VISIBLE
                isEnabled = false
                img_tip_logo!!.visibility = View.GONE
                progress!!.visibility = View.VISIBLE
                tv_tips?.text = context.getText(R.string.network_loading).toString()
            }
            LoadStatus.onlyText -> {
                isEnabled = false
                visibility = View.VISIBLE
                img_tip_logo!!.visibility = View.GONE
                progress!!.visibility = View.GONE
                tv_tips?.text = context.getText(R.string.network_complete).toString()
            }
            LoadStatus.finish -> visibility = View.GONE
        }
    }

    fun setOnReloadListener(listener: LoadingTipListener?) {
        onReloadListener = listener
    }

    /**
     * 重新尝试接口
     */
    interface LoadingTipListener {
        fun reload()
    }
}
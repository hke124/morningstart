package com.hke.morningstart.widget

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler

class SearchLayout : RecyclerView {
    @JvmOverloads
    constructor(
        context: Context,
        attributeSet: AttributeSet? = null,
        defStyleAttr: Int = 0
    ) : super(
        context,
        attributeSet,
        defStyleAttr
    )

    private val mLayoutManager: LinearLayoutManager = SearchLayoutManager(context);

    private fun init() {

    }

    fun scrollSearchContainerToTop() {
        mLayoutManager.scrollToPositionWithOffset(0, 0)
    }
}

internal class SearchLayoutManager(context: Context?) :
    LinearLayoutManager(context) {
    override fun onLayoutChildren(recycler: Recycler, state: RecyclerView.State) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }
}